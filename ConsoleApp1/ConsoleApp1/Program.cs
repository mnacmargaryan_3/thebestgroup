﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //var student = new Student()
            //{
            //    Name = "Poghos",
            //    LastName = "Petrosyan",
            //    Age = 25
            //};

            //var serialized = JsonConvert.SerializeObject(student);

            //File.WriteAllText("D:\\person.json", serialized);
            var serialized = File.ReadAllText("D:\\person.json");

            var student = JsonConvert.DeserializeObject<Student>(serialized);
        }
    }

    class Student
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
